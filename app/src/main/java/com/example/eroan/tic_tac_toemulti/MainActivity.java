package com.example.eroan.tic_tac_toemulti;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


public class MainActivity extends AppCompatActivity {
    int h, w;
    int xPos, yPos;
    Canvas mCanvas;
    Paint mPaint;
    Bitmap mBitmap;
    DemoView demoview;
    Path circlePath, crossPath;
    String ip, choix;
    ServerSocket mServerSocket;
    String [][] cases = new String[3][3];
    WifiManager wm;
    ClientThread clientThread;
    Thread serverThread;
    Socket tempClientSocket;
    Boolean serverConnected = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        w = Resources.getSystem().getDisplayMetrics().widthPixels;
        h = Resources.getSystem().getDisplayMetrics().heightPixels;

        super.onCreate(savedInstanceState);
        //Paint (correspond à l'outil de dessin)
        mPaint = new Paint();
        mPaint.setStyle(Style.STROKE);
        //Path (correspond au tracé du dessin)
        circlePath = new Path();
        crossPath = new Path();
        //Intent
        Bundle from = getIntent().getExtras();
        choix = (String) from.get("choix");
        ip = (String) from.get("ip");
        System.out.println(choix + "\n" + ip);
        if(choix.equalsIgnoreCase("online")){
            if(ip!=null){
                clientThread = new ClientThread();
                Thread client = new Thread(clientThread);
                client.start();
                Log.d("Info", "Created client Thread");
            }else{
                wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                serverConnected = false;
                serverThread = new Thread(new ServerThread());
                serverThread.start();
                Toast.makeText(getApplicationContext(), Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress()),Toast.LENGTH_LONG).show();

            }
        }

        //DemoView
        demoview = new DemoView(this);
        demoview.setLayoutParams(new LayoutParams(w, w));
        setContentView(demoview);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != serverThread) {
            serverThread.interrupt();
            serverThread = null;
        }
        if (null != clientThread) {
            clientThread = null;
        }
    }

    private class DemoView extends View {
        Integer lastX, lastY, turn=0;


        public DemoView(Context context) {
            super(context);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            mBitmap = Bitmap.createBitmap(w, h, Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            mPaint.setColor(Color.BLACK);
            mPaint.setStrokeWidth(2);
            mPaint.setTextSize(16 * getResources().getDisplayMetrics().density);
            mPaint.setStrokeWidth(1);
            canvas.drawLine(0, w / 3, w, w / 3, mPaint);
            canvas.drawLine(0, 2 * w / 3, w, 2 * w / 3, mPaint);
            canvas.drawLine(w / 3, 0, w / 3, w, mPaint);
            canvas.drawLine(2 * w / 3, 0, 2 * w / 3, w, mPaint);
            mPaint.setStrokeWidth(2);
            mPaint.setColor(Color.BLUE);
            canvas.drawPath(crossPath,mPaint);
            mPaint.setColor(Color.RED);
            canvas.drawPath(circlePath,mPaint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    return true;
                case MotionEvent.ACTION_UP:
                    lastX = (int) event.getX();
                    lastY = (int) event.getY();
                    drawForm();

                    return true;
                case MotionEvent.ACTION_MOVE:
                    return super.onTouchEvent(event);
            }
            return false;
        }
        public void drawForm() {
            if (lastX <= w / 3) {
                xPos = 0;
            } else if (lastX > w / 3 && lastX <= (2 * w / 3)) {
                xPos = 1;
            } else {
                xPos = 2;
            }
            if (lastY <= w / 3) {
                yPos = 0;
            } else if (lastY > w / 3 && lastY <= (2 * w / 3)) {
                yPos = 1;
            } else {
                yPos = 2;
            }
            //En ligne et serveur
            if(choix.equals("online") && ip==null){
                if(turn%2==0 && serverConnected){
                    fill(xPos, yPos);
                }
            //En ligne et client
            } else if(choix.equals("online") && ip!=null){
                if(turn%2!=0){
                    fill(xPos,yPos);
                }
            }else {
                fill(xPos, yPos);
            }
        }

        public void fill(int x, int y){
            if (cases[x][y]==null) {
                if (turn % 2 != 0) {
                    circlePath.addCircle((2 * (x + 1) * (w / 6) - (w / 6)), (2 * (y + 1) * (w / 6) - (w / 6)), w / 6, Path.Direction.CW);
                    cases[x][y] = "Cercle";
                } else {
                    crossPath.moveTo((x * (w / 3)), (y * (w / 3)));
                    crossPath.lineTo(((x + 1) * (w / 3)), ((y + 1) * (w / 3)));
                    crossPath.moveTo(((x + 1) * (w / 3)), (y * (w / 3)));
                    crossPath.lineTo((x * (w / 3)), ((y + 1) * (w / 3)));
                    cases[x][y] = "Croix";
                }
                turn++;
                invalidate();
                if(ip==null && choix.equals("online") && turn%2 == 1) {
                    Log.d("Info", "fill : Called serverSend");
                    sendMessage(x + "," +  y);
                }else if(ip != null && choix.equals("online") && turn%2 == 0){
                    clientThread.sendMessage(x + "," + y);
                }
                if(verif()==1 || turn==9){
                    Toast.makeText(getContext(),"Reset...", Toast.LENGTH_SHORT).show();
                    init();
                }
            }
        }

        public int verif(){
            if(cases[0][0]==cases[1][1] && cases[1][1]==cases[2][2] && cases[0][0]!=null) {
                Toast.makeText(getContext(), "Le gagnant est : " + cases[0][0], Toast.LENGTH_LONG).show();
                return 1;
            }if(cases[0][2]==cases[1][1] && cases[1][1]==cases[2][0] && cases[1][1]!=null) {
                Toast.makeText(getContext(), "Le gagnant est : " + cases[0][2], Toast.LENGTH_LONG).show();
                return 1;
            }
            for(int i = 0; i < 3; i++){
                if(cases[i][0]==cases[i][1] && cases[i][1]==cases[i][2] && cases[i][0]!=null){
                    Toast.makeText(getContext(),"Le gagnant est : " + cases[i][0], Toast.LENGTH_LONG).show();
                    return 1;
                }if(cases[0][i]==cases[1][i] && cases[1][i]==cases[2][i] && cases[0][i]!=null){
                    Toast.makeText(getContext(),"Le gagnant est : " + cases[0][i], Toast.LENGTH_LONG).show();
                    return 1;
                }
            }
            return 0;
        }

        public void init(){
            turn=0;
            crossPath.reset();
            circlePath.reset();
            for(int i = 0;i<3;i++){
                for(int j = 0;j<3;j++){
                    cases[i][j]=null;
                }
            }
        }
    }

    /*----------------SERVEUR----------------*/
    private void sendMessage(String message) {
        try {
            if (null != tempClientSocket) {
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(tempClientSocket.getOutputStream())),
                        true);
                out.println(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ServerThread implements Runnable {
        public void run() {
            Socket socket;
            try {
                mServerSocket = new ServerSocket(6000);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != mServerSocket) {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        socket = mServerSocket.accept();
                        serverConnected = true;
                        CommunicationThread commThread = new CommunicationThread(socket);
                        new Thread(commThread).start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    class CommunicationThread implements Runnable {
        private Socket clientSocket;
        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
            tempClientSocket = clientSocket;
            try {
                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("CommunicationThread","Server Started...");
        }

        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    String read = input.readLine();
                    Log.i("Communication Thread", "Message Received from Client : " + read);
                    final String finalRead = read;
                    Thread t = new Thread() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    demoview.fill(Integer.parseInt(finalRead.split(",")[0]), Integer.parseInt(finalRead.split(",")[1]));
                                }
                            });
                        }
                    };
                    t.start();
                    if (null == read || "Disconnect".contentEquals(read)) {
                        Thread.interrupted();
                        read = "Client Disconnected";
                        Log.d("Info",read);
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
   /*----------------CLIENT----------------*/
    class ClientThread implements Runnable{
        private Socket socket;
        private BufferedReader input;

        @Override
        public void run() {
            try{
                InetAddress serverAddr = InetAddress.getByName(ip);
                socket = new Socket(serverAddr, 6000);
                while (!Thread.currentThread().isInterrupted()) {
                    Log.i("Info, ClientThread", "Waiting for message from server...");
                    this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String message = input.readLine();
                    Log.i("Info, ClientThread", "Message received from the server : " + message);
                    final String finalMessage = message;
                    Thread t = new Thread() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    demoview.fill(Integer.parseInt(finalMessage.split(",")[0]), Integer.parseInt(finalMessage.split(",")[1]));
                                }
                            });
                        }
                    };
                    t.start();
                    if (null == message || "Disconnect".contentEquals(message)) {
                        Thread.interrupted();
                        message = "Server Disconnected.";
                        break;
                    }
                }
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        void sendMessage(String message) {
            try {
                if (null != socket) {
                    PrintWriter out = new PrintWriter(new BufferedWriter(
                            new OutputStreamWriter(socket.getOutputStream())),
                            true);
                    out.println(message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}