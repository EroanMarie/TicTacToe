# Tic-Tac-Toe Android game

```
        88                                                              
  ,d    ""              ,d                            ,d                
  88                    88                            88                
MM88MMM 88  ,adPPYba, MM88MMM ,adPPYYba,  ,adPPYba, MM88MMM ,adPPYba,    ,adPPYba,  
  88    88 a8"     ""   88    ""     `Y8 a8"     ""   88   a8"     "8a  a8P_____88  
  88    88 8b           88    ,adPPPPP88 8b           88   8b       d8  8PP"""""""  
  88,   88 "8a,   ,aa   88,   88,    ,88 "8a,   ,aa   88,  "8a,   ,a8"  "8b,   ,aa  
  "Y888 88  `"Ybbd8"'   "Y888 `"8bbdP"Y8  `"Ybbd8"'   "Y888 `"YbbdP"'    `"Ybbd8"'  
 ```




### Objectives

The objective of this project is to discover Android programming by developping a simple Android game.


### Hardware requirements

In order to run the app we need :
- 1 Android device running on **at least v4.4 KitKat (API 19)**
- Internet access for multiplayer mode

*Note : To play multiplayer both Android devices must be on the same network*


### Installation and usage

[**Download and install the game here!**](https://gitlab.com/EroanMarie/TicTacToe/-/blob/73c8e61182c5962bb57cafa0bdd858b42ba12818/TicTacToe.apk)


<br>

### Contact

[Eroan Marie](https://gitlab.com/EroanMarie)

[Pr. Sunflower](https://gitlab.com/prsunflower)

<br>

### We hope that you'll enjoy the game!

```
                                                         ;
                          |            |             ..""
                 .____,   |            |    ____   ."
                . \  / ,  |            |  ,' __ `.;
                |`-  -'|  |            | / ,'  ..'\
                |,-  -.|  |            | | | ;" | |
                ' /__\ `  |            | \ ;'__,' /
                 '    `   |            |  :.____,'
             _____________|____________|_._____________
                 .____,   |    ____    :'
                . \  / ,  |  ,' __ ..."|
                |`-  -'|  | / ,' :". \ |
                |,-  -.|  | | |..) | | |
                ' /__\ `  | \ ;.__,' / |
                 '    `   | .".____,'  |
             _____________:'___________|_______________
                  ____  ""|            |   .____,
                ,' __ `:  |            |  . \  / ,
               / ,'  ." \ |            |  |`-  -'|
               | | .; | | |            |  |,-  -.|
               \ .:__,' / |            |  ' /__\ `
                :.____,'  |            |   '    `
              :"          |            |
           ..'
```
