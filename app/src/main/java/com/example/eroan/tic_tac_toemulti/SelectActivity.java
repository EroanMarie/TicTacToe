package com.example.eroan.tic_tac_toemulti;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Eroan on 12/03/2018.
 */

public class SelectActivity extends AppCompatActivity implements View.OnClickListener{
    Button local, online;
    String ip;
    Intent onlineIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        local = (Button) findViewById(R.id.local);
        online = (Button) findViewById(R.id.online);
        local.setOnClickListener(this);
        online.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.online:
                final CheckBox hote = (CheckBox) findViewById(R.id.hote);
                onlineIntent = new Intent(this, MainActivity.class);
                onlineIntent.putExtra("choix","online");
                if(!hote.isChecked()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    final EditText edittext = new EditText(SelectActivity.this);
                    alert.setMessage("Entrer l'adresse IP de l'hôte");
                    alert.setTitle("Adresse IP");

                    alert.setView(edittext);

                    alert.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ip = edittext.getText().toString();
                            if(!ip.isEmpty()) {
                                onlineIntent.putExtra("ip", String.valueOf(ip));
                                startActivity(onlineIntent);
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "L'adresse IP ne peut pas être vide", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    alert.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // what ever you want to do with No option.
                        }
                    });

                    alert.show();
                }else{
                    startActivity(onlineIntent);
                }
                break;
            case R.id.local:
                Intent localIntent = new Intent(this, MainActivity.class);
                localIntent.putExtra("choix","local");
                startActivity(localIntent);
                break;
            default:
                break;
        }
    }
}
